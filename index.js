var http = require('http');
var url = require('url');
var fs = require('fs');
var server = http.createServer();

server.on('request', (req, res) => {
  const uri = url.parse(req.url).pathname;

  fs.readFile('./.public/index.html', 'utf-8', (err, data) => {
    res.writeHead(200, {
        'content-Type': 'text/html'
    });
    res.write(data);
    res.end();
  });
});

server.listen(process.env.npm_package_config_port);