#!/bin/bash

set -ue

. util.sh

ASCIIDOCTOR_VERSION=$(tag_docker_asciidoctor)
NODE_VERSION=$(cat .node-version)
NODE_VERSION_MAJOR=${NODE_VERSION:0:2}
YARN_VERSION=$(get_yarn_version)

echo ASCIIDOCTOR_VERSION=${ASCIIDOCTOR_VERSION}
echo NODE_VERSION=${NODE_VERSION}
echo NODE_VERSION_MAJOR=${NODE_VERSION_MAJOR}
echo YARN_VERSION=${YARN_VERSION}

if [ -z ${CONTAINER_TEST_IMAGE} ]; then
  export CONTAINER_TEST_IMAGE=dac-runtime:latest
fi
if [ -z ${PROJECT_PATH} ]; then
  export PROJECT_PATH=asciidoctor
fi

docker build --pull \
  --build-arg BASE_VERSION=$ASCIIDOCTOR_VERSION \
  --build-arg NODE_VERSION=${NODE_VERSION} \
  --build-arg NODE_VERSION_MAJOR=${NODE_VERSION_MAJOR} \
  --build-arg YARN_VERSION=${YARN_VERSION} \
  --build-arg PROJECT_PATH=${PROJECT_PATH} \
  -t ${CONTAINER_TEST_IMAGE} .
