function get_ruby_version() {
  RUBY_VERSION=$(docker run --rm -it asciidoctor/docker-asciidoctor:1.5.8 ruby --version | awk -F p '{print $1}' | awk '{print $2}' | tr -d '\n')
  echo ${RUBY_VERSION} > .ruby-version
  cat .ruby-version
}

function get_yarn_version() {
  NODE_VERSION=$(cat .node-version)
  docker pull node:${NODE_VERSION}-alpine >/dev/null
  YARN_VERSION=$(docker inspect node:${NODE_VERSION}-alpine | jq -r '.[].Config.Env[] | select(match("YARN_VERSION"))' | awk -F = '{print $2}')
  echo ${YARN_VERSION}
}

function tag_docker_asciidoctor() {
  docker pull asciidoctor/docker-asciidoctor:latest >/dev/null
  ASCIIDOCTOR_VERSION=$(docker inspect asciidoctor/docker-asciidoctor:latest | jq -r '.[].Config.Env[] | select(match("ASCIIDOCTOR_VERSION"))' | awk -F = '{print $2}')
  if [ -z "`docker image ls -q --filter "reference=asciidoctor/docker-asciidoctor:${ASCIIDOCTOR_VERSION}"`" ]; then
    docker tag asciidoctor/docker-asciidoctor:latest asciidoctor/docker-asciidoctor:${ASCIIDOCTOR_VERSION}
  fi
  echo ${ASCIIDOCTOR_VERSION}
}