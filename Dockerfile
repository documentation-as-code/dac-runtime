ARG BASE_VERSION=latest
ARG NODE_VERSION=10.15.3
ARG PROJECT_PATH=asciidoctor

FROM ubuntu:16.04 as node_modules

ENV CXX=g++-4.8

RUN apt-get update -qq
RUN apt-get install -y curl apt-transport-https sudo $CXX

# Use NodeJS v10.x. https://github.com/yarnpkg/yarn/issues/6914
ARG NODE_VERSION_MAJOR=10
RUN curl -sL https://deb.nodesource.com/setup_$NODE_VERSION_MAJOR.x | sudo -E bash -
RUN apt-get install -y nodejs

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -qq
RUN apt-get install -y yarn

RUN apt-get install -y build-essential

WORKDIR /tmp
COPY yarn.lock yarn.lock
COPY package.json package.json
RUN yarn install 

FROM node:$NODE_VERSION-alpine as node
FROM $PROJECT_PATH/docker-asciidoctor:${BASE_VERSION}

LABEL maintainer="aradgyma@gmail.com"

WORKDIR /app
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock

RUN gem install --no-document bundler \
  && bundle install
RUN asciidoctor-pdf-cjk-kai_gen_gothic-install

ARG YARN_VERSION=1.13.0
COPY --from=node /opt/yarn-v$YARN_VERSION /opt/yarn
COPY --from=node /usr/local/bin/node /usr/local/bin/
RUN ln -s /opt/yarn/bin/yarn /usr/local/bin/yarn \
  && ln -s /opt/yarn/bin/yarn /usr/local/bin/yarnpkg

WORKDIR /app
COPY --from=node_modules /tmp/node_modules ./node_modules
COPY --from=node_modules /tmp/package.json ./package.json
COPY --from=node_modules /tmp/yarn.lock ./yarn.lock
COPY test.sh test.sh